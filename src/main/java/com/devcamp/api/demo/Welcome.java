package com.devcamp.api.demo;

import org.springframework.web.bind.annotation.CrossOrigin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class Welcome {
    @CrossOrigin
    @GetMapping("/devcamp-welcome")
    public String nice() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now= new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }
}


